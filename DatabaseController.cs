﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using System.Security.Cryptography;

namespace ToDo
{
    class DatabaseController
    {
        protected MongoClient Client;
        protected IMongoDatabase Database;
        protected static DatabaseController Instance = null;
        public bool Connect()
        {
            // Создай новый кластер на https://cloud.mongodb.com/
            // Используя Free tier.

            string password = "KSxzBZcnpezQ3wms";
            string database = "develop";
            string Uri = @"mongodb+srv://app:" + password + "@cluster0-rheuh.azure.mongodb.net/test?retryWrites=true&w=majority";

            this.Client = new MongoClient(Uri);
            this.Database = this.Client.GetDatabase(database);

            return true;
        }

        protected DatabaseController()
        {
            this.Connect();
        }

        public static DatabaseController getInstance()
        {
            if (DatabaseController.Instance == null)
            {
                DatabaseController.Instance = new DatabaseController();
            }

            return DatabaseController.Instance;
        }

        public IMongoDatabase GetDatabase()
        {
            return this.Database;
        }

        public bool UserExists(string login, string password_hash = null)
        {
            var users = DatabaseController.getInstance().GetDatabase().GetCollection<BsonDocument>("users");

            var builder = Builders<BsonDocument>.Filter;
            FilterDefinition<BsonDocument>[] filterAnd = {
                builder.Eq("login", login)
            };

            if (password_hash != null)
            {
                filterAnd.Append(builder.Eq("password", password_hash));
            }

            return users.CountDocuments(builder.And(filterAnd)) > 0;
        }

        public void AddUser(string login, string password_hash)
        {
            var users = DatabaseController.getInstance().GetDatabase().GetCollection<BsonDocument>("users");

            BsonDocument user = new BsonDocument 
            {
                { "login", login },
                { "password", password_hash },
                { "created", DateTime.UtcNow.ToString("o") } // ISO Date
            };

            users.InsertOne(user);
        }

        public static string HashPassword(string password_plain)
        {
            SHA256 hash = SHA256.Create();
            byte[] hashed_bytes = hash.ComputeHash(Encoding.UTF8.GetBytes(password_plain));
            string hashed = "";
            for(uint i=0; i< hashed_bytes.Length; i++)
            {
                hashed += hashed_bytes[i].ToString("x2");
            }

            return hashed;
        }
    }
}
