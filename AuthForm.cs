﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Driver;
using MongoDB.Bson;

namespace ToDo
{
    public partial class AuthForm : Form
    {
        public MainForm main_form;

        public AuthForm()
        {
            InitializeComponent();

            this.main_form = new MainForm();
            this.main_form.auth_form = this;
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            DatabaseController db = DatabaseController.getInstance();

            string password_hash = DatabaseController.HashPassword(this.PasswordText.Text);
            if (!db.UserExists(this.LoginText.Text, password_hash))
            {
                MessageBox.Show("User not found or login/password is incorrect", "Auth error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                this.Hide();
                this.main_form.Show();
            }
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            DatabaseController db = DatabaseController.getInstance();
            string password_hash = DatabaseController.HashPassword(this.PasswordText.Text);
            if (db.UserExists(this.LoginText.Text))
            {
                MessageBox.Show("The user already exists", "Auth error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            db.AddUser(this.LoginText.Text, password_hash);

            MessageBox.Show("User has been registered", "Registration", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void AuthForm_Load(object sender, EventArgs e)
        {

        }
    }
}
